<?php
require_once "config/db_config.php";
require_once "Classes/Gadget.php";

$gadgetId = htmlspecialchars(stripslashes(trim($_POST['gadgetId'])), ENT_QUOTES, "UTF-8");

Gadget::deleteGadget($gadgetId, $db);
header("Location:index.php");
?>