<?php
try {
    $db = new PDO('mysql:host=localhost;dbname=shop', 'admin', '11111');
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $db->exec('SET NAMES "utf8"');
} catch (Exception $e) {
    $message = 'Технические проблемы, пожалуйста зайдите позже...' . $e->getMessage();
    die($message);
}
?>