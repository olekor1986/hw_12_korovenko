<?php
require_once "config/db_config.php";
require_once "Classes/Gadget.php";

$gadgetObj = Gadget::showAll($db);
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Главная</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="#">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
<div class="container header navbar-expand">
        <nav class="navbar navbar-light fixed-top">
            <a class="navbar-brand" href="index.php">
                <img src="img/phone.png" width="30" height="30" class="d-inline-block align-top" alt="">
                Интернет-магазин гаджетов
            </a>
        <ul class="navbar-nav bd-navbar-nav flex-row">
            <li class="nav-item">
                <a class="nav-link" href="index.php">Главная</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="createGadget.php">Добавить новый гаджет</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="seeder_db.php">Заполнить базу данных</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="create_db.php">Создать базу данных</a>
            </li>
        </ul>
        </nav>
</div>
<div class="container content">
    <div class="row centered">
        <?php foreach ($gadgetObj as $key => $object):?>
        <div class="col-lg-4 col-md-6 col-sm-12">
            <div class="card">
                <div class="card-body">
                    <h4><?=$object->getTitle()?></h4>
                    <p><?=$object->getPrice() . ' грн.'?></p>
                    <form action="showGadget.php" method="post">
                        <input type="hidden" name="gadgetId" value="<?=$object->getId()?>">
                        <button class="btn btn-outline-dark btn-sm">Просмотр</button>
                    </form>
                </div>
            </div>
        </div>
        <?php endforeach;?>
    </div>
</div>
<div class="container footer text-center">
    <p>Интернет-магазин гаджетов (c) 2019</p>
</div>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>
