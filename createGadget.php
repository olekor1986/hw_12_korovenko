<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Добавить</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="#">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
<div class="container header navbar-expand">
    <nav class="navbar navbar-light fixed-top">
        <a class="navbar-brand" href="index.php">
            <img src="img/phone.png" width="30" height="30" class="d-inline-block align-top" alt="">
            Интернет-магазин гаджетов
        </a>
        <ul class="navbar-nav bd-navbar-nav flex-row">
            <li class="nav-item">
                <a class="nav-link" href="index.php">Главная</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="createGadget.php">Добавить новый гаджет</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="seeder_db.php">Заполнить базу данных</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="create_db.php">Создать базу данных</a>
            </li>
        </ul>
    </nav>
</div>
<div class="container content">
    <form action="addGadget.php" method="post">
        <div class="form-group">
            <label for="validationCustom01">Название
                <input type="text" class="form-control" id="validationCustom01" required autofocus name="title" placeholder="Например: iPhone XR">
            </label>
        </div>
        <div class="form-group">
            <label for="validationCustom02">Цена, грн
                <input type="text" class="form-control" id="validationCustom02" required name="price">
            </label>
        </div>
        <div class="form-group">
            <label for="textarea01">Описание
                <textarea type="text" class="form-control" id="textarea01" required name="description"></textarea>
            </label>
        </div>
        <div class="form-group">
            <label>Тип
                <select name="type" class="form-control" required>
                    <option value="" disabled selected hidden>Выберите тип</option>
                    <option value="laptop">Ноутбук</option>
                    <option value="phone">Смартфон</option>
                    <option value="watch">Часы</option>
                </select>
            </label>
        </div>
        <button class="btn btn-outline-dark btn-sm">Сохранить</button>
    </form>
</div>
<div class="container footer text-center">
    <p>Интернет-магазин гаджетов (c) 2019</p>
</div>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>
