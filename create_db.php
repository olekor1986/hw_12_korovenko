<?php
require_once "config/db_config.php";

try {
    $sql = "CREATE TABLE gadgets (
        id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
        title VARCHAR (255) NOT NULL,
        price FLOAT NOT NULL,
        description VARCHAR (1000),
        type VARCHAR (255) NOT NULL
        ) DEFAULT CHARACTER SET utf8 ENGINE=InnoDB" ;
    $db->exec($sql);
} catch (Exception $e) {
    $message = 'Не удается создать таблицу базы данных.' . $e->getMessage();
    die($message);
}
header("Location:index.php");
?>