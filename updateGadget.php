<?php
require_once "config/db_config.php";
require_once "Classes/Gadget.php";

$gadgetId =  htmlspecialchars(stripslashes(trim($_POST['gadgetId'])), ENT_QUOTES, "UTF-8");
$gadgetTitle = htmlspecialchars(stripslashes(trim($_POST['title'])), ENT_QUOTES, "UTF-8");
$gadgetPrice = htmlspecialchars(stripslashes(trim($_POST['price'])), ENT_QUOTES, "UTF-8");
$gadgetDescription = htmlspecialchars(stripslashes(trim($_POST['description'])), ENT_QUOTES, "UTF-8");
$gadgetType = htmlspecialchars(stripslashes(trim($_POST['type'])), ENT_QUOTES, "UTF-8");

$gadgetObj = new Gadget($gadgetTitle, $gadgetPrice, $gadgetDescription, $gadgetType);
$gadgetObj->setId($gadgetId);

$gadgetObj->updateGadget($db);

header("Location:index.php");
?>